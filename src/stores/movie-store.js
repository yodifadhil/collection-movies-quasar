import { defineStore } from "pinia";
import uniqid from "uniqid";

export const useMoviesStore = defineStore("movies", {
  state: () => ({
    movies: [],
    moviesSample: [
      {
        id: 1,
        title: "Title 1",
        director: "Director 1",
        summary:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        genres: ["Sci-fi", "Horror"],
      },
      {
        id: 2,
        title: "Title 2",
        director: "Director 2",
        summary: "Lorem ipsum dolor sit amet",
        genres: ["Drama", "Horror"],
      },
      {
        id: 3,
        title: "Title 3",
        director: "Director 1",
        summary:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        genres: ["Sci-fi", "Drama"],
      },
      {
        id: 4,
        title: "Title 4",
        director: "Director 2",
        summary:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        genres: ["Drama", "Horror"],
      },
      {
        id: 5,
        title: "Title 5",
        director: "Director 1",
        summary: "",
        genres: ["Action", "Horror", "Drama"],
      },
      {
        id: 2,
        title: "Title 6",
        director: "Director 6",
        summary:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        genres: ["Drama", "Animation"],
      },
    ],
    genres: ["Drama", "Action", "Animation", "Sci-fi", "Horror"],
    searchQuery: "",
  }),
  getters: {
    filteredMovies: (state) =>
      state.movies.filter(
        (movie) =>
          movie.title.toLowerCase().indexOf(state.searchQuery.toLowerCase()) >
          -1
      ),
    movieById: (state) => (id) => state.movies.find((movie) => movie.id == id),
  },
  actions: {
    addMovie(newMovie) {
      const id = uniqid();
      newMovie.id = id;

      this.movies.push(newMovie);
    },
    updateMovie(id, newData) {
      this.movies = this.movies.map((movie) =>
        movie.id == id ? newData : movie
      );
    },
    deleteMovie(id) {
      const arrIdx = this.movies.findIndex((movie) => movie.id == id);
      if (arrIdx > -1) {
        this.movies.splice(arrIdx, 1);
      }
    },
  },
});
