import MainLayoutVue from "layouts/MainLayout.vue";
import HomePage from "pages/IndexPage.vue";
import DetailPage from "pages/DetailPage.vue";
import NewPage from "pages/NewPage.vue";
import AboutPageVue from "pages/AboutPage.vue";

const routes = [
  {
    path: "/",
    component: MainLayoutVue,
    children: [
      { path: "", name: "homePage", component: HomePage },
      { path: "detail/:id", name: "detailPage", component: DetailPage },
      { path: "add-new", name: "newPage", component: NewPage },
      { path: "about", name: "aboutPage", component: AboutPageVue },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
